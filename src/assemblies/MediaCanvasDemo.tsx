'use client'

import s from "../style/assemblies/VideoCanvasModule.module.css"
import flexStyle from "../style/utilitary/Flex.module.css"
import MediaCanvas, {AssetInfo} from "@/components/MediaCanvas";
import {Button, Paper, TextField} from "@mui/material";
import {useRef, useState} from "react";
import {clsx} from "clsx";

export default function MediaCanvasDemo() {
  /* TODO Replace with useImperativeHandle */
  const getItemsInfoFunctionRef = useRef(() => []);
  const playAllVideosFunctionRef = useRef(() => {});
  const stopAllVideosFunctionRef = useRef(() => {});
  
  const [items, setItems] = useState([]);
  const [newUrl, setNewUrl] = useState('');
  const [errorText, setErrorText] = useState('');
  const [logs, setLogs] = useState([] as AssetInfo[]);
  
  function onDeleteItem(url: string) {
    setItems(items.filter(item => item !== url));
  }
  function onMoveItemUp(url: string) {
    const index = items.indexOf(url);
    let newItems = [...items];
    newItems.splice(index, 1);
    setItems([...newItems.slice(0, index + 1), url, ...items.slice(index + 2)]);
  }
  
  function onMoveItemDown(url: string) {
    const index = items.indexOf(url);
    let newItems = [...items];
    newItems.splice(index, 1);
    setItems([...newItems.slice(0, index - 1), url, ...newItems.slice(index - 1)]);
  }
  
  function addNewCanvasItem() {
    function isUrl(string) {
      let url;
      try {
        url = new URL(string);
      } catch (_) {
        return false;
      }
      return url.protocol === "http:" || url.protocol === "https:";
    }
    
    if (!newUrl) {
      setErrorText("No url to use");
      return
    } else if (items.indexOf(newUrl) !== -1) {
      setErrorText("Item with same url already exist on canvas");
      return
    } else if (!isUrl(newUrl)) {
      setErrorText("This url is not valid");
      return
    }
    
    setItems([...items, newUrl])
    setNewUrl('')
  }
  
  function logAssetsInformation() {
    setLogs(getItemsInfoFunctionRef.current());
  }
  
  return (
    <Paper className={clsx(s.videoCanvasComponent, flexStyle.column, flexStyle.gap2)}>
      <div className={[flexStyle.row, flexStyle.gap1].join(' ')}>
        <TextField label={"New video/image resource url"}
                   className={flexStyle.grow}
                   variant={"outlined"}
                   value={newUrl}
                   onInput={e => setNewUrl((e.target as HTMLInputElement).value)}
        />
        <Button variant={"contained"} onClick={addNewCanvasItem}>
          Add
        </Button>
      </div>
      {errorText ? (
        <strong className={s.errorText}>
          {errorText}
        </strong>
      ) : null}
      <MediaCanvas getItemsInfoFunctionRef={getItemsInfoFunctionRef}
                   playAllVideosFunctionRef={playAllVideosFunctionRef}
                   stopAllVideosFunctionRef={stopAllVideosFunctionRef}
                   onDeleteItem={onDeleteItem}
                   onMoveItemUp={onMoveItemUp}
                   onMoveItemDown={onMoveItemDown}
                   style={{height: '80vh'}}
                   items={items}
      />
      <div className={clsx(flexStyle.row, flexStyle.gap1, flexStyle.childrenCanGrow)}>
        <Button variant={"contained"} color={"success"} onClick={() => playAllVideosFunctionRef.current()}>
          Play all videos
        </Button>
        <Button variant={"contained"} color={"error"} onClick={() => stopAllVideosFunctionRef.current()}>
          Stop all videos
        </Button>
      </div>
      <Button variant={"contained"} onClick={logAssetsInformation}>
        Log assets information
      </Button>
      {logs.length ? (
        <div>
          <strong>Logs:</strong>
          <ol>
            {
              logs.map(item => (
                <li key={item.url}>
                  {item.url}
                  <ul>
                    <li>X: {item.x}px</li>
                    <li>Y: {item.y}px</li>
                    <li>Width: {item.width}px</li>
                    <li>Height: {item.height}px</li>
                  </ul>
                </li>
              ))
            }
          </ol>
        </div>
      ) : null}
    </Paper>
  )
}