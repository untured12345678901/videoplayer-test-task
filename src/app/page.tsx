import style from "../style/Home.module.css"
import MediaCanvasDemo from "@/assemblies/MediaCanvasDemo";

export default function Home() {
  return (
    <div className={style.home}>
      <p className={style.request}>
        Request:<br/>
        Create a React app that:<br/>
        [+] has an input that receives a URL to a video or an image (later in the text - "asset");<br/>
        [+] has a button that adds the asset from the input to the "canvas". The canvas is not necessarily a
        &lt;canvas/&gt; element, it can be anything. Note: videos on canvas should be playable of course;<br/>
        [+] allows users to move the assets freely around the canvas with their mouse.
        The assets can be stacked on top of each other;<br/>
        [+] allows users to resize the assets freely with their mouse;<br/>
        [+] has another button that logs the information about each asset that includes their current x, y,
        width, height (pixels or %). Alternatively, the information can be display in the UI all the time;<br/>
        [+] all assets should keep their original aspect ratio including after resize<br/>
        <br/>
        Additionally:<br/>
        [+] URL validation<br/>
        [+] deletion of the assets from the canvas<br/>
        [+] global playback to play/pause all videos on the canvas simultaneously.<br/>
      </p>
      <MediaCanvasDemo/>
    </div>
  )
}
