'use client'

import s from "../style/components/MediaCanvas.module.css"
import AspectRatioIcon from '@mui/icons-material/AspectRatio';
import DragIndicatorIcon from '@mui/icons-material/DragIndicator';
import DeleteIcon from '@mui/icons-material/Delete';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import {clsx} from "clsx";
import {CSSProperties, memo, MutableRefObject, useEffect, useRef, useState} from "react";
import {Paper, Tooltip} from "@mui/material";
import flexStyle from "../style/utilitary/Flex.module.css"

type CanvasItemProps = {
  url: string,
  itemsData: Map<number | string, CanvasItemData>,
  dragging: boolean,
  indexInItemsArray: number,
  itemsArrayLength: number,
  onResizeStart: (url: string, resizeButtonRef: MutableRefObject<HTMLElement>, aspectRatio: number) => void,
  onMoveStart: (url: string, dragButtonRef: MutableRefObject<HTMLElement>) => void,
  onDelete: (url: string) => void,
  onMoveItemUp: (url: string) => void,
  onMoveItemDown: (url: string) => void
}

function isImageUrl(url: string) {
  return Boolean(url.match(/\.(jpeg|jpg|gif|png)$/));
}

function CanvasItem({
  url,
  itemsData,
  dragging,
  indexInItemsArray,
  itemsArrayLength,
  onResizeStart = (() => {}) as Function,
  onMoveStart = (() => {}) as Function,
  onDelete = (() => {}) as Function,
  onMoveItemUp = (() => {}) as Function,
  onMoveItemDown = (() => {}) as Function,
}: CanvasItemProps) {
  const dragButtonRef = useRef(null);
  const resizeButtonRef = useRef(null);
  const itemRef = useRef(null);
  
  const item = itemsData.get(url);
  const style: CSSProperties = {
    width: item.width
  };
  const containerStyle: CSSProperties = {
    left: item.x + 'px',
    top: item.y + 'px',
    zIndex: indexInItemsArray
  };
  
  function onResizeButtonClicked() {
    const {width, height} = (itemRef.current as HTMLElement).getBoundingClientRect();
    const aspectRatio = height / width;
    onResizeStart(url, resizeButtonRef as MutableRefObject<HTMLElement>, aspectRatio);
  }
  
  return (
    <div className={clsx(s.canvasItem, dragging ? s.currentlyDraggedCanvasItem : null)} style={containerStyle}>
      {dragging ? <div className={s.catchMouseEventOverlay}/> : null}
      {isImageUrl(url) ? (
        <img ref={itemRef}
             className={s.media}
             style={style}
             src={url}
             alt={url}
        />
      ) : (
        <video ref={itemRef}
               className={s.media}
               style={style}
               src={url}
               controls
        />
      )}
      <Tooltip title={"Move"} placement={"top"}>
        <DragIndicatorIcon ref={dragButtonRef}
                           className={clsx(s.iconContainer, s.serviceIcon, s.moveIcon)}
                           onMouseDown={() => onMoveStart(url, dragButtonRef as MutableRefObject<HTMLElement>)}
        />
      </Tooltip>
      <Tooltip title={"Resize"} placement={"top"}>
        <AspectRatioIcon ref={resizeButtonRef}
                         className={clsx(s.iconContainer, s.serviceIcon, s.resizeIcon)}
                         onMouseDown={onResizeButtonClicked}
        />
      </Tooltip>
      <div className={clsx(flexStyle.row, flexStyle.childrenCanGrow, s.iconContainer, s.secondaryIconsContainer)}>
        {indexInItemsArray < (itemsArrayLength - 1) ? (
          <Tooltip title={"Move one layer up"} placement={"top"}>
            <ArrowRightAltIcon className={clsx(s.serviceIcon, s.layerIconUp)} onClick={() => onMoveItemUp(url)}/>
          </Tooltip>
        ) : null}
        {itemsArrayLength > 1 && indexInItemsArray > 0 ? (
          <Tooltip title={"Move one layer down"} placement={"top"}>
            <ArrowRightAltIcon className={clsx(s.serviceIcon, s.layerIconDown)} onClick={() => onMoveItemDown(url)}/>
          </Tooltip>
        ) : null}
        <Tooltip title={"Delete"} placement={"top"}>
          <DeleteIcon className={clsx(s.serviceIcon, s.deleteIcon)} onMouseDown={() => onDelete(url)}/>
        </Tooltip>
      </div>
    </div>
  )
}

enum DragType {
  NONE = "NONE",
  RESIZE = "RESIZE",
  MOVE = "MOVE"
}

type CanvasItemData = {
  x: number,
  y: number,
  width: string,
}

type MediaCanvasProps = {
  items: string[],
  style?: CSSProperties,
  getItemsInfoFunctionRef?: MutableRefObject<() => AssetInfo[]>,
  playAllVideosFunctionRef?: MutableRefObject<() => void>,
  stopAllVideosFunctionRef?: MutableRefObject<() => void>,
  onDeleteItem: (url: string) => void,
  onMoveItemUp: (url: string) => void,
  onMoveItemDown: (url: string) => void
}

export type AssetInfo = {
  url: string,
  x: number,
  y: number,
  width: number,
  height: number
}

function getDefaultDragInfo() {
  return {
    type: DragType.NONE as DragType,
    itemUrl: '' as string,
    dragButtonRef: null as MutableRefObject<HTMLElement>,
    resizeButtonRef: null as MutableRefObject<HTMLElement>,
    aspectRatio: 0
  }
}

export default function MediaCanvas({
   style,
   items,
   /* TODO Replace with useImperativeHandle */
   getItemsInfoFunctionRef,
   playAllVideosFunctionRef,
   stopAllVideosFunctionRef,
   onDeleteItem = (() => {}) as Function,
   onMoveItemUp = (() => {}) as Function,
   onMoveItemDown = (() => {}) as Function
}: MediaCanvasProps) {
  const [dragInfo, setDragInfo] = useState(getDefaultDragInfo());
  const [itemsData, setItemsData] = useState(new Map<number | string, CanvasItemData>());
  const canvasElement = useRef<HTMLElement>(null as HTMLElement);
  
  /* TODO Replace with useImperativeHandle */
  if (getItemsInfoFunctionRef) {
    getItemsInfoFunctionRef.current = getItemsInfo;
  }
  if (playAllVideosFunctionRef) {
    playAllVideosFunctionRef.current = playAllVideos;
  }
  if (stopAllVideosFunctionRef) {
    stopAllVideosFunctionRef.current = stopAllVideos;
  }
  
  function onResizeStart(itemUrl: string, resizeButtonRef: MutableRefObject<HTMLElement>, aspectRatio: number) {
    setDragInfo({
      ...dragInfo,
      itemUrl,
      type: DragType.RESIZE,
      resizeButtonRef,
      aspectRatio
    });
  }
  function onMoveStart(itemUrl: string, dragButtonRef: MutableRefObject<HTMLElement>) {
    setDragInfo({
      ...dragInfo,
      itemUrl,
      type: DragType.MOVE,
      dragButtonRef
    });
  }
  function onDrag(e: MouseEvent) {
    const url = dragInfo.itemUrl;
    const type = dragInfo.type
    
    if (type === DragType.RESIZE) {
      const resizeButtonRect = dragInfo.resizeButtonRef.current.getBoundingClientRect();
      const newItemsData = structuredClone(itemsData);
      
      const maxWidth = canvasElement.current.getBoundingClientRect().height / dragInfo.aspectRatio;
      const newWidth = Math.min(e.pageX - itemsData.get(url).x - resizeButtonRect.width, maxWidth);
      
      newItemsData.get(url).width = `max(${newWidth}px, 5em)`;
      
      setItemsData(newItemsData);
    } else if (type === DragType.MOVE) {
      const dragButtonRect = dragInfo.dragButtonRef.current.getBoundingClientRect();
      const {
        x: canvasX,
        y: canvasY,
        width: canvasWidth,
        height: canvasHeight} = canvasElement.current.getBoundingClientRect();
      const newItemsData = structuredClone(itemsData);
      
      newItemsData.set(url, {
        ...itemsData.get(url),
        x: Math.min(Math.max(e.clientX - canvasX, 0), canvasWidth - dragButtonRect.width),
        y: Math.min(Math.max(e.clientY - canvasY, 0), canvasHeight - dragButtonRect.height)
      });
      
      setItemsData(newItemsData);
    }
  }
  function onDragEnd() {
    setDragInfo(getDefaultDragInfo());
  }
  
  function getItemsInfo(): AssetInfo[] {
    const canvasRect = canvasElement.current.getBoundingClientRect()
    
    return ([...canvasElement.current.children] as HTMLElement[])
      .map(child => {
        const rect = child.getBoundingClientRect();
        return {
          url: (child.querySelector('img, video') as (HTMLImageElement | HTMLVideoElement)).src,
          x: rect.x - canvasRect.x,
          y: rect.y - canvasRect.y,
          width: rect.width,
          height: rect.height
        }
      });
  }
  function getAllVideoElements() {
    return ([...canvasElement.current.children] as HTMLElement[])
      .map(child => child.querySelector('video') as HTMLVideoElement)
      .filter(el => el);
  }
  function playAllVideos() {
    getAllVideoElements().forEach(videoElement => videoElement.play());
  }
  function stopAllVideos() {
    getAllVideoElements().forEach(videoElement => videoElement.pause());
  }
  
  useEffect(() => {
    const newItemsSet: Set<number | string> = items.reduce((set, item) => set.add(item), new Set());
    
    const newItemsData = new Map<number | string, CanvasItemData>();
    for (const key of newItemsSet.keys()) {
      if (itemsData.has(key)) {
        newItemsData.set(key, itemsData.get(key));
      } else {
        newItemsData.set(key, {
          x: 0,
          y: 0,
          width: '20em'
        });
      }
    }
    
    setItemsData(newItemsData)
  }, [items]);
  useEffect(() => {
    if (dragInfo.type === DragType.NONE) {
      window.removeEventListener('mousemove', onDrag);
      window.removeEventListener('mouseup', onDragEnd);
    } else {
      window.addEventListener('mousemove', onDrag);
      window.addEventListener('mouseup', onDragEnd);
    }
    
    return () => {
      if (dragInfo.type !== DragType.NONE) {
        window.removeEventListener('mousemove', onDrag);
        window.removeEventListener('mouseup', onDragEnd);
      }
    }
  });
  
  return (
    <Paper ref={canvasElement as any} className={s.canvas} style={style}>
      {items
        .filter(item => itemsData.has(item))
        .map((item, index) => (
          <CanvasItem
            key={item}
            url={item}
            indexInItemsArray={index}
            itemsArrayLength={items.length}
            itemsData={itemsData}
            dragging={dragInfo.itemUrl === item}
            onResizeStart={onResizeStart}
            onMoveStart={onMoveStart}
            onDelete={onDeleteItem}
            onMoveItemUp={onMoveItemUp}
            onMoveItemDown={onMoveItemDown}
          />
        ))}
    </Paper>
  )
}